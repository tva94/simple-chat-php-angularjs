<?php
$config = [
    "db_host" => "localhost",
    "db_database" => "chat.local",
    "db_user" => "chat.local",
    "db_password" => "",
    "db_charset" => "utf8",
    "db_provider" => "mysql",
    "allow_register" => true,
    "new_user_is_active" => true,
    "server_name" => "test",
    "admin_email" => "test@local",
];
$resolved_host = gethostbyname($config["db_host"]);

$dsn = "{$config["db_provider"]}:host={$resolved_host};dbname={$config["db_database"]};charset={$config["db_charset"]}";

$opt = [
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES   => false,
    /*PDO::ATTR_PERSISTENT         => true,*/
];

$pdo = new PDO($dsn, $config["db_user"], $config["db_password"], $opt);

// This line set mysql idle timeout to maximum -- 1 year
$pdo->query('SET wait_timeout=31536000')->execute();


function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}


if(isset($_REQUEST['action'])) {
    if(($_REQUEST['action'] == "login") || ($_REQUEST['action'] == "register") || ($_REQUEST['action'] == "server_info")) {
        if ($_REQUEST['action'] == "login") {
            if(isset($_REQUEST['login']) && isset($_REQUEST['password'])) {
                if($stmt_login = $pdo->prepare("SELECT `id`, `user_key` FROM `users` WHERE `login` = :login AND `password` = :password AND `active` = 1")) {
                    $stmt_login->bindValue("login", $_REQUEST['login']);
                    $stmt_login->bindValue("password", $_REQUEST['password']);
                    $stmt_login->execute();
                    if ($row = $stmt_login->fetch()) {
                        echo json_encode(["status" => "ok", "user_id" => $row['id'], "user_key" => $row['user_key']]);
                    } else {
                        echo json_encode(["status" => "error", "message" => "Login credentials are invalid"]);
                    }
                }
            } else {
                echo json_encode(["status" => "error", "message" => "You need to send login and password"]);
            }
        }

        if ($_REQUEST['action'] == "register") {
            if($config['allow_register']) {
                if(isset($_REQUEST['login']) && isset($_REQUEST['full_name']) && isset($_REQUEST['password'])) {
                    if ($stmt_register = $pdo->prepare("INSERT INTO `users`(`login`, `full_name`, `password`, `user_key`, `active`, `admin`) VALUES (:login, :full_name, :password, :user_key, :active, :admin)")) {

                        $stmt_register->bindValue("login", $_REQUEST['login']);
                        $stmt_register->bindValue("full_name", $_REQUEST['full_name']);
                        $stmt_register->bindValue("password", $_REQUEST['password']);
                        $stmt_register->bindValue("user_key", generateRandomString(64));
                        $stmt_register->bindValue("active", $config['new_user_is_active'] ? '1' : '0');
                        $stmt_register->bindValue("admin", 0);

                        $stmt_register->execute();

                        if ($stmt_register->rowCount() == 1) {
                            echo json_encode(["status" => "ok", "user registered with id" => $pdo->lastInsertId('id')]);
                        } else {
                            echo json_encode(["status" => "error", "message" => "User not registered"]);
                        }
                    }
                } else {
                    echo json_encode(["status" => "error", "message" => "Not all params given"]);
                }

            } else {
                echo json_encode(["status" => "error", "message" => "Regstration is disabled"]);
            }

        }

        if ($_REQUEST['action'] == "server_info") {
            echo json_encode(["status" => "ok", "info" => [
                "allow_register" => $config['allow_register'],
                "new_user_is_active" => $config['new_user_is_active'],
                "server_name" => $config['server_name'],
                "admin_email" => $config['admin_email']
            ]]);
        }

    }  else {
        if(isset($_REQUEST['user_id']) && isset($_REQUEST['user_key'])) {
            if($stmt_user = $pdo->prepare("SELECT `id`, `login`, `password`, `full_name`, `user_key`, `admin` FROM `users` WHERE `id` = :user_id AND `user_key` = :user_key AND `active` = 1")) {
                $stmt_user->bindValue("user_id", $_REQUEST['user_id']);
                $stmt_user->bindValue("user_key", $_REQUEST['user_key']);
                $stmt_user->execute();
                if ($row_user = $stmt_user->fetch()) {
                    // User is verified, now we can work
                    if($_REQUEST['action'] == "getUserData")  {
                        echo json_encode(["status" => "ok",
                            "user_id" => $row_user['id'],
                            "login" => $row_user['login'],
                            "full_name" => $row_user['full_name'],
                            "admin" => $row_user['admin']
                        ]);
                    }

                    if($_REQUEST['action'] == "getUsersList")  {
                        if($stmt_usersList = $pdo->prepare("SELECT `users`.`id`, `login`, `full_name`, `admin`, `active` FROM `users`")) {
                            $stmt_usersList->execute();
                            //$users = $stmt_usersList->fetchAll();
                            $users = [];
                            while ($user = $stmt_usersList->fetch()) {
                                $users[$user['id']] = $user;
                            }

                            echo json_encode(["status" => "ok", "usersList" => $users ]);
                        }
                    }

                    if($_REQUEST['action'] == "getLastMessages")  {
                        // find user_id => last message list
                        if($stmt_last_msgs = $pdo->prepare("SELECT `users`.`id` AS `user_id`, `m1`.`id`, `m1`.`id_sender`, `m1`.`id_reciever`, `m1`.`send_time`, `m1`.`type`, `m1`.`data`, `m1`.`read_time` FROM `users` LEFT JOIN `messages` `m1` ON (((`users`.`id` = `m1`.`id_sender`) AND (:my_id1 = `m1`.`id_reciever`)) OR ((`users`.`id` = `m1`.`id_reciever`) AND (:my_id2 = `m1`.`id_sender`))) LEFT JOIN `messages` `m2` ON ((`m1`.`id` < `m2`.`id`) AND (((`users`.`id` = `m2`.`id_sender`) AND (:my_id3 = `m2`.`id_reciever`)) OR ((`users`.`id` = `m2`.`id_reciever`) AND (:my_id4 = `m2`.`id_sender`)))) WHERE `m2`.`id` IS NULL")) {
                            $stmt_last_msgs->bindValue("my_id1", $row_user['id']);
                            $stmt_last_msgs->bindValue("my_id2", $row_user['id']);
                            $stmt_last_msgs->bindValue("my_id3", $row_user['id']);
                            $stmt_last_msgs->bindValue("my_id4", $row_user['id']);
                            $stmt_last_msgs->execute();

                            $last_messages = [];

                            while ($message = $stmt_last_msgs->fetch()) {
                                $last_messages[$message['user_id']] = $message;
                            }

                            echo json_encode(["status" => "ok", "last_messages" => $last_messages ]);
                        }
                    }

                    if($_REQUEST['action'] == "sendMessage")  {
                        if(isset($_REQUEST['target_id']) && isset($_REQUEST['type']) && isset($_REQUEST['data'])) {
                            if($stmt_sendMessage = $pdo->prepare("INSERT INTO `messages`(`id_sender`, `id_reciever`, `send_time`, `type`, `data`, `read_time`) VALUES (:id_sender, :id_reciever, :send_time, :type, :data, :read_time)")) {
                                $stmt_sendMessage->bindValue("id_sender", $row_user['id']);
                                $stmt_sendMessage->bindValue("id_reciever", $_REQUEST['target_id']);
                                $stmt_sendMessage->bindValue("send_time", microtime(true) * 1000);
                                $stmt_sendMessage->bindValue("type", $_REQUEST['type']);
                                $stmt_sendMessage->bindValue("data", $_REQUEST['data']);
                                $stmt_sendMessage->bindValue("read_time", ($_REQUEST['target_id'] == $row_user['id']) ? (microtime(true) * 1000) : null);
                                $stmt_sendMessage->execute();

                                if($stmt_sendMessage->rowCount() == 1) {
                                    echo json_encode(["status" => "ok", "newMessageId" => $pdo->lastInsertId('id')]);
                                } else {
                                    echo json_encode(["status" => "error", "message" => "Message not sent"]);
                                }
                            }
                        } else {
                            echo json_encode(["status" => "error", "message" => "Not all parameters given to send message"]);
                        }
                    }

                    if($_REQUEST['action'] == "getMessages")  {
                        if(isset($_REQUEST['target_id'])) {
                            $offset = isset($_REQUEST['msg_offset']) ? $_REQUEST['msg_offset'] : 0;
                            $limit = isset($_REQUEST['msg_limit']) ? $_REQUEST['msg_limit'] : 20;
                            $max_id = isset($_REQUEST['msg_max_id']) ? $_REQUEST['msg_max_id'] : 0; // to load old messages with offset
                            if($stmt_getMessages = $pdo->prepare("SELECT `id`, `id_sender`, `id_reciever`, `send_time`, `type`, `data`, `read_time` FROM `messages` WHERE (((`id_sender` = :my_id1) AND (`id_reciever` = :target_id1)) OR ((`id_sender` = :target_id2) AND (`id_reciever` = :my_id2))) AND ((`id` < :max_id1) OR (:max_id2 = 0)) ORDER BY `id` DESC LIMIT :limit OFFSET :offset")) {
                                $stmt_getMessages->bindValue("offset", $offset);
                                $stmt_getMessages->bindValue("limit", $limit);
                                $stmt_getMessages->bindValue("max_id1", $max_id);
                                $stmt_getMessages->bindValue("max_id2", $max_id);
                                $stmt_getMessages->bindValue("my_id1", $row_user['id']);
                                $stmt_getMessages->bindValue("my_id2", $row_user['id']);
                                $stmt_getMessages->bindValue("target_id1", $_REQUEST['target_id']);
                                $stmt_getMessages->bindValue("target_id2", $_REQUEST['target_id']);

                                $messages = [];
                                $stmt_getMessages->execute();
                                //$messages = $stmt_getMessages->fetchAll();
                                while ($message = $stmt_getMessages->fetch()) {
                                    $messages[$message['id']] = $message;
                                }

                                echo json_encode(["status" => "ok", "messages" => $messages ]);
                            }
                        } else {
                            echo json_encode(["status" => "error", "message" => "Need target id"]);
                        }
                    }

                    // Use this function for pagination
                    if($_REQUEST['action'] == "getMessagesCount")  {
                        if(isset($_REQUEST['target_id'])) {
                            if($stmt_getMessages = $pdo->prepare("SELECT COUNT(*) AS cnt FROM `messages` WHERE (((`id_sender` = :my_id1) AND (`id_reciever` = :target_id1)) OR ((`id_sender` = :target_id2) AND (`id_reciever` = :my_id2)))")) {
                                $stmt_getMessages->bindValue("my_id1", $row_user['id']);
                                $stmt_getMessages->bindValue("my_id2", $row_user['id']);
                                $stmt_getMessages->bindValue("target_id1", $_REQUEST['target_id']);
                                $stmt_getMessages->bindValue("target_id2", $_REQUEST['target_id']);

                                $stmt_getMessages->execute();
                                if($data = $stmt_getMessages->fetch()) {
                                    echo json_encode(["status" => "ok", "messages_cnt" => $data["cnt"] ]);
                                } else {
                                    echo json_encode(["status" => "error", "message" => "Some problems"]);
                                }



                            }
                        } else {
                            echo json_encode(["status" => "error", "message" => "Need target id"]);
                        }
                    }

                    // Need to correct wait for new messages
                    if($_REQUEST['action'] == "getLastMessageId")  {
                        if($stmt_lastMessageId = $pdo->prepare("SELECT MAX(`id`) AS `max_id` FROM `messages` WHERE `id_sender` = :my_id1 OR `id_reciever` = :my_id2")) {
                            $stmt_lastMessageId->bindValue("my_id1", $row_user['id']);
                            $stmt_lastMessageId->bindValue("my_id2", $row_user['id']);
                            $stmt_lastMessageId->execute();
                            $lastMessageid = null;

                            if ($lastMessadeIdData = $stmt_lastMessageId->fetch()) {
                                $lastMessageid = $lastMessadeIdData["max_id"];
                            }

                            echo json_encode(["status" => "ok", "lastMessageId" => $lastMessageid ]);
                        }
                    }

                    if($_REQUEST['action'] == "waitForMessages")  {
                        if(isset($_REQUEST['msg_min_id'])) {
                            // Wait for ANY message for current user
                            $time_read = isset($_REQUEST['time_read']) ? $_REQUEST['time_read'] : microtime(true) * 1000;
                            $limit = isset($_REQUEST['msg_limit']) ? $_REQUEST['msg_limit'] : 20;
                            $min_id = isset($_REQUEST['msg_min_id']) ? $_REQUEST['msg_min_id'] : 0; // last known messages id

                            if($stmt_getMessages = $pdo->prepare("SELECT `id`, `id_sender`, `id_reciever`, `send_time`, `type`, `data`, `read_time` FROM `messages` WHERE ((`id_sender` = :my_id1) OR (`id_reciever` = :my_id2)) AND ((`id` > :min_id) OR (`read_time` > :read_time)) ORDER BY `id` DESC LIMIT :limit")) {
                                $stmt_getMessages->bindValue("limit", $limit);
                                $stmt_getMessages->bindValue("min_id", $min_id);
                                $stmt_getMessages->bindValue("my_id1", $row_user['id']);
                                $stmt_getMessages->bindValue("my_id2", $row_user['id']);
                                $stmt_getMessages->bindValue("read_time", $time_read);

                                $messages = [];
                                $start_time = microtime(true);
                                while ((count($messages) == 0) && (microtime(true) - $start_time < 10)) {
                                    $stmt_getMessages->execute();
                                    //$messages = $stmt_getMessages->fetchAll();
                                    while ($message = $stmt_getMessages->fetch()) {
                                        $messages[$message['id']] = $message;
                                    }

                                    $stmt_getMessages->closeCursor();
                                    //sleep(1);
                                    usleep(250000);
                                }


                                echo json_encode(["status" => "ok", "messages" => $messages ]);
                            }
                        } else {
                            echo json_encode(["status" => "error", "message" => "Need max known message id"]);
                        }
                    }

                    if($_REQUEST['action'] == "readMessages")  {
                        if(isset($_REQUEST['msg_id']) && isset($_REQUEST['user_chat_id'])) {
                            $start_time = microtime(true) * 1000;
                            if($stmt_readMessages = $pdo->prepare("UPDATE `messages` SET `read_time`=:read_time WHERE ((`id` <= :msg_id1) OR (:msg_id2 = 0)) AND (`read_time` IS NULL) AND ((`id_sender` = :target_id1) AND (`id_reciever` = :my_id1))")) {
                                $stmt_readMessages->bindValue("my_id1", $row_user['id']);
                                $stmt_readMessages->bindValue("target_id1", $_REQUEST['user_chat_id']);
                                $stmt_readMessages->bindValue("read_time", $start_time);
                                $stmt_readMessages->bindValue("msg_id1", $_REQUEST['msg_id']);
                                $stmt_readMessages->bindValue("msg_id2", $_REQUEST['msg_id']);

                                $stmt_readMessages->execute();


                                echo json_encode(["status" => "ok", "messages_readed" => $stmt_readMessages->rowCount() ]);
                            }
                        } else {
                            echo json_encode(["status" => "error", "message" => "Need target id and message id"]);
                        }
                    }

                    if($_REQUEST['action'] == "updateMyProfile")  {

                        $new_full_name = isset($_REQUEST['new_full_name']) ? $_REQUEST['new_full_name'] : $row_user['full_name'];
                        $new_password = (!empty($_REQUEST['new_password'])) ? $_REQUEST['new_password'] : $row_user['password'];
                        $update_key = isset($_REQUEST['update_key']) ? ($_REQUEST['update_key'] == "1") : false;

                        if($stmt_updateMyUser = $pdo->prepare("UPDATE `users` SET `full_name`=:new_full_name, `password`=:new_password, `user_key`=:new_user_key WHERE `id`=:my_id")) {
                            $stmt_updateMyUser->bindValue("my_id", $row_user['id']);
                            $stmt_updateMyUser->bindValue("new_full_name", $new_full_name);
                            $stmt_updateMyUser->bindValue("new_password", $new_password);
                            $stmt_updateMyUser->bindValue("new_user_key", $update_key ? generateRandomString(64) : $row_user['user_key']);

                            $stmt_updateMyUser->execute();

                            echo json_encode(["status" => "ok", "user profile updated" => $stmt_updateMyUser->rowCount() ]);
                        }

                    }

                    if($_REQUEST['action'] == "createUser")  {
                        if($row_user['admin'] == "1") {
                            if(isset($_REQUEST['login']) && isset($_REQUEST['full_name']) && isset($_REQUEST['password'])) {
                                $active = isset($_REQUEST['active']) ? $_REQUEST['active'] : "1";
                                $admin = isset($_REQUEST['admin']) ? $_REQUEST['admin'] : "0";
                                $user_key = generateRandomString(64);

                                if($stmt_createUser = $pdo->prepare("INSERT INTO `users`(`login`, `full_name`, `password`, `user_key`, `active`, `admin`) VALUES (:login, :full_name, :password, :user_key, :active, :admin)")) {

                                    $stmt_createUser->bindValue("login", $_REQUEST['login']);
                                    $stmt_createUser->bindValue("full_name", $_REQUEST['full_name']);
                                    $stmt_createUser->bindValue("password", $_REQUEST['password']);
                                    $stmt_createUser->bindValue("user_key", $user_key);
                                    $stmt_createUser->bindValue("active", $active);
                                    $stmt_createUser->bindValue("admin", $admin);

                                    $stmt_createUser->execute();

                                    if($stmt_createUser->rowCount() == 1) {
                                        echo json_encode(["status" => "ok", "user created with id" =>  $pdo->lastInsertId('id')]);
                                    } else {
                                        echo json_encode(["status" => "error", "message" => "User not created"]);
                                    }

                                }
                            } else {
                                echo json_encode(["status" => "error", "message" => "Not all parameters given"]);
                            }

                        } else {
                            echo json_encode(["status" => "error", "message" => "You haven't rights to do this"]);
                        }
                    }

                    if($_REQUEST['action'] == "updateUser")  {
                        if($row_user['admin'] == "1") {

                            if(isset($_REQUEST['id'])) {
                                $result = [];

                                //  && isset($_REQUEST['login']) && isset($_REQUEST['full_name']) && isset($_REQUEST['password']) && isset($_REQUEST['active']) && isset($_REQUEST['admin']) && isset($_REQUEST['new_user_key'])
                                if(isset($_REQUEST['login'])) {
                                    if ($stmt_updateUserLogin = $pdo->prepare("UPDATE `users` SET `login`=:login WHERE `id`=:my_id")) {
                                        $stmt_updateUserLogin->bindValue("my_id", $_REQUEST['id']);
                                        $stmt_updateUserLogin->bindValue("login", $_REQUEST['login']);
                                        $stmt_updateUserLogin->execute();
                                        if($stmt_updateUserLogin->rowCount() == 1) {
                                            $result['login'] = 'ok';
                                        }
                                    }
                                }

                                if(isset($_REQUEST['full_name'])) {
                                    if ($stmt_updateFullName = $pdo->prepare("UPDATE `users` SET `full_name`=:full_name WHERE `id`=:my_id")) {
                                        $stmt_updateFullName->bindValue("my_id", $_REQUEST['id']);
                                        $stmt_updateFullName->bindValue("full_name", $_REQUEST['full_name']);
                                        $stmt_updateFullName->execute();
                                        if($stmt_updateFullName->rowCount() == 1) {
                                            $result['full_name'] = 'ok';
                                        }
                                    }
                                }

                                if(isset($_REQUEST['password']) && !empty($_REQUEST['password'])) {
                                    if ($stmt_updatePassword = $pdo->prepare("UPDATE `users` SET `password`=:password WHERE `id`=:my_id")) {
                                        $stmt_updatePassword->bindValue("my_id", $_REQUEST['id']);
                                        $stmt_updatePassword->bindValue("password", $_REQUEST['password']);
                                        $stmt_updatePassword->execute();
                                        if($stmt_updatePassword->rowCount() == 1) {
                                            $result['password'] = 'ok';
                                        }
                                    }
                                }

                                if(isset($_REQUEST['active'])) {
                                    if ($stmt_updateActive = $pdo->prepare("UPDATE `users` SET `active`=:active WHERE `id`=:my_id")) {
                                        $stmt_updateActive->bindValue("my_id", $_REQUEST['id']);
                                        $stmt_updateActive->bindValue("active", $_REQUEST['active']);
                                        $stmt_updateActive->execute();
                                        if($stmt_updateActive->rowCount() == 1) {
                                            $result['active'] = 'ok';
                                        }
                                    }
                                }

                                if(isset($_REQUEST['admin'])) {
                                    if ($stmt_updateAdmin = $pdo->prepare("UPDATE `users` SET `admin`=:admin WHERE `id`=:my_id")) {
                                        $stmt_updateAdmin->bindValue("my_id", $_REQUEST['id']);
                                        $stmt_updateAdmin->bindValue("admin", $_REQUEST['admin']);
                                        $stmt_updateAdmin->execute();
                                        if($stmt_updateAdmin->rowCount() == 1) {
                                            $result['admin'] = 'ok';
                                        }
                                    }
                                }

                                if($_REQUEST['new_user_key'] == '1') {
                                    if ($stmt_updateUserKey = $pdo->prepare("UPDATE `users` SET `user_key`=:user_key WHERE `id`=:my_id")) {
                                        $stmt_updateUserKey->bindValue("my_id", $_REQUEST['id']);
                                        $stmt_updateUserKey->bindValue("user_key", generateRandomString(64));
                                        $stmt_updateUserKey->execute();
                                        if($stmt_updateUserKey->rowCount() == 1) {
                                            $result['new_user_key'] = 'ok';
                                        }
                                    }
                                }


                                echo json_encode(["status" => "ok", "user updated" => $result]);
                            } else {
                                echo json_encode(["status" => "error", "message" => "Not all parameters given"]);
                            }

                        } else {
                            echo json_encode(["status" => "error", "message" => "You haven't rights to do this"]);
                        }
                    }

                    if($_REQUEST['action'] == "deleteUser")  {
                        if($row_user['admin'] == "1") {
                            if(isset($_REQUEST['id'])) {

                                if($stmt_deleteUser = $pdo->prepare("DELETE FROM `users` WHERE `id`=:id")) {

                                    $stmt_deleteUser->bindValue("id", $_REQUEST['id']);

                                    $stmt_deleteUser->execute();

                                    if($stmt_deleteUser->rowCount() == 1) {
                                        echo json_encode(["status" => "ok"]);
                                    } else {
                                        echo json_encode(["status" => "error", "message" => "User not deleted"]);
                                    }

                                }
                            } else {
                                echo json_encode(["status" => "error", "message" => "Not all parameters given"]);
                            }

                        } else {
                            echo json_encode(["status" => "error", "message" => "You haven't rights to do this"]);
                        }
                    }


                } else {
                    echo json_encode(["status" => "error", "action" => "logout", "message" => "Session is invalid"]);
                }
            }

        } else {
            echo json_encode(["status" => "error", "action" => "logout", "message" => "You need to login"]);
        }
    }

} else {
    echo json_encode(["status" => "error", "message" => "No action"]);
}



?>
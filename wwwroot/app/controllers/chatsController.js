app.controller("chatsController", function ($scope, $rootScope, $location, $routeParams, ChatCore) {
    $scope.chatId = null;
    $scope.messageToSend = "";
    $scope.usersListFilter = "";

    if($routeParams.chatId) {
        $scope.chatId = $routeParams.chatId;
        ChatCore.getMessages($scope.chatId);
    }

    $scope.sendMessage = function () {
        if($scope.messageToSend.length > 0) {
            ChatCore.sendMessage($scope.chatId, $scope.messageToSend);
            $scope.messageToSend = "";
        }
    };

    $scope.loadMoreMessages = function () {
        var min_id = Math.min.apply(null, Object.keys($rootScope.messages[$scope.chatId]));

        ChatCore.getMessages($scope.chatId, min_id, 20);
        /*for(var id in messages[chatId]) {

        }*/
       // ChatCore.sendMessage($scope.chatId, $scope.messageToSend);
    };

    $scope.readChatMessages = function () {
        if($scope.chatId) {
            ChatCore.readMessages(0, $scope.chatId)
        }
    };

    $scope.$on('$viewContentLoaded', function(){
        //Here your view content is fully loaded !!
        $scope.readChatMessages();
    });

    $scope.$watch('messageToSend', function (newVal, oldVal) {
        if((oldVal == "") && (newVal != "")) {
            $scope.readChatMessages();
        }
    });

    /*$scope.$watch(
        function() { return $rootScope.messages },
        function(data) {
            console.log(data);
        }
    );*/

});
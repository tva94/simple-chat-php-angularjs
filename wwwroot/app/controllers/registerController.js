app.controller("registerController", function ($scope, $rootScope, $location, ChatCore) {

    $scope.input_user_login = "";
    $scope.input_user_full_name = "";
    $scope.input_user_password = "";
    $scope.input_user_password_repeat = "";
    $scope.errorMessage = null;
    $scope.infoMessage = null;
    $scope.waitForResponse = false;

    $scope.register = function () {
        if($scope.input_user_password != $scope.input_user_password_repeat ) {
            $scope.errorMessage = "Passwords don't match";
            return;
        }

        if($scope.input_user_password == "" ) {
            $scope.errorMessage = "You can't use empty password";
            return;
        }

        $scope.waitForResponse = true;
        ChatCore.registerUser($scope.input_user_login, $scope.input_user_full_name, $scope.input_user_password, function (result) {
            $scope.waitForResponse = false;
            if(result) {
                if($rootScope.new_user_is_active) {
                    $scope.infoMessage = "User is created. You can login with your login and password";
                } else  {
                    $scope.infoMessage = "User is created. Tou can login after admin will activate your account. Contact admin for more information";
                }

            } else {
                $scope.errorMessage = "There are some problems with registration";
            }
        });
        $scope.input_user_password = "";
        $scope.input_user_password_repeat = "";
    };


});
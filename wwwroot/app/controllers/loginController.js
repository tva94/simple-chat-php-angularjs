app.controller("loginController", function ($scope, $location, ChatCore) {

    $scope.waitForResponse = false;
    $scope.errorMessage = null;

    var loginCallbackFunc = function (result) {
        $scope.waitForResponse = false;
        if(result) {
            $location.path("/menu");
        } else {
            $scope.inputPassword = "";
            $scope.errorMessage="Error";
        }
    };

    $scope.onLoginClick = function() {
        $scope.waitForResponse = true;
        ChatCore.login($scope.inputLogin, $scope.inputPassword, loginCallbackFunc)
    };
});
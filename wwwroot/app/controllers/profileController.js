app.controller("profileController", function ($scope, $rootScope, $location, ChatCore) {

    $scope.input_user_login = $rootScope.user_login;
    $scope.input_user_full_name = $rootScope.user_fullName;
    $scope.input_user_admin = $rootScope.user_admin ? "1" : "0";
    $scope.input_user_active = "1"; // If user not active - it can't be here
    $scope.input_user_key_upd = "0";
    $scope.input_user_password = "";
    $scope.updated = false;

    $scope.updateUser = function () {
        $scope.updated = false;
        ChatCore.updateMyProfile($scope.input_user_full_name, $scope.input_user_password, $scope.input_user_key_upd, function (result) {
            if(result) {
                $scope.updated = true;
            }
        });
        $scope.input_user_password = "";
    };


});
app.controller("adminController", function ($scope, $rootScope, $routeParams, $location, ChatCore) {
    $scope.usersListFilter = "";
    if($routeParams.userId) {
        if($routeParams.userId == "new") {
            $scope.mode = "userCreate";
            $scope.input_user_id = null;
            $scope.input_user_login = "";
            $scope.input_user_full_name = "";
            $scope.input_user_password = "";
            $scope.input_user_admin = "0";
            $scope.input_user_active = "1";
            $scope.input_user_update_key = "1";
        } else {
            if($rootScope.users.hasOwnProperty($routeParams.userId)) {
                $scope.mode = "userUpdate";
                $scope.input_user_id = $rootScope.users[$routeParams.userId].id;
                $scope.input_user_login = $rootScope.users[$routeParams.userId].login;
                $scope.input_user_full_name = $rootScope.users[$routeParams.userId].full_name;
                $scope.input_user_password = "";
                $scope.input_user_admin = $rootScope.users[$routeParams.userId].admin.toString();
                $scope.input_user_active = $rootScope.users[$routeParams.userId].active.toString();
                $scope.input_user_update_key = "0";
            } else {
                $location.path('/admin');
            }
        }
    } else {
        $scope.mode = "userList";
    }

    $scope.formSubmit = function () {
        if($scope.mode = "userCreate") {
            $scope.createUserStore();
        }

        if($scope.mode = "userUpdate") {
            $scope.updateUserStore();
        }
    };

    $scope.createUserStart = function () {
        $location.path('/admin/new');
    };

    $scope.createUserStore = function () {
        ChatCore.createUser($scope.input_user_login, $scope.input_user_full_name, $scope.input_user_password, $scope.input_user_active, $scope.input_user_admin, function (result) {
            $location.path('/admin');
        });
    };

    $scope.updateUserSelect = function (id) {
        $location.path('/admin/' + id);
    };

    $scope.updateUserStore = function () {
        ChatCore.updateUser($scope.input_user_id, $scope.input_user_login, $scope.input_user_full_name, $scope.input_user_password, $scope.input_user_active, $scope.input_user_admin, $scope.input_user_update_key, function (result) {
            $location.path('/admin');
        });
    };

    $scope.deleteUser = function (id) {
        ChatCore.deleteUser(id, function (result) {
            $location.path('/admin');
        });
    };
});
'use strict';

// Define the `phonecatApp` module
var app = angular.module('chat-app', [
    'ngRoute'
]).run(function ($rootScope, $location, ChatCore) {
    $rootScope.connectOK = false;

    $rootScope.users = [];

    $rootScope.messages = {};

    $rootScope.chats = {};

    $rootScope.unreadChats = 0;
    $rootScope.unreadMessages = 0;

    // Hook redirections and check login
    $rootScope.$on('$locationChangeStart', function (event, next, current) {
        if(!ChatCore.isLoggedIn()) {
            var allowedUrls = ["/", "/login", "/register"];
            if(allowedUrls.indexOf($location.path()) == -1) {
                $location.path("/login");
            }
        } else {
            if(!ChatCore.isAdmin()) {
                if($location.path().indexOf("/admin") == 0) {
                    $location.path("/login");
                }
            }
        }
    });

});

app.config(['$httpProvider', function($httpProvider) {
    $httpProvider.defaults.useXDomain = true;
    //$httpProvider.defaults.withCredentials = true;
    delete $httpProvider.defaults.headers.common["X-Requested-With"];
}
]);
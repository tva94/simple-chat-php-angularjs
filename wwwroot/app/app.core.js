app.factory("ChatCore", function ($http, $location, $rootScope, config) {
    var _isLoggedIn = false;
    var _isAdmin = false;

    function demo () {

    }

    function processRequestError(data) {
        if ((data.status == "error") && (data.action == "logout")) {
            logout();
        }
    }

    function getUserData(callback, noRunMgsWait) {
        var allowedUrls = ["/", "/login"];
        $rootScope.user_id = localStorage.getItem('user_id');
        $rootScope.user_key = localStorage.getItem('user_key');

        if (($rootScope.user_id) && ($rootScope.user_key)) {
            // If data is set -- it allow to load page, data will come later.
            // On porblems we will redirect to login

            $rootScope.user_loggedin = true;
            _isLoggedIn = true;
            _isAdmin = true;

            $http({
                method: 'POST',
                url: config.apiUrl,
                timeout: 5000,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param({
                    "action" : "getUserData",
                    "user_id" : $rootScope.user_id,
                    "user_key" : $rootScope.user_key,
                })
            }).then(function successCallback(response) {
                var data = response.data;
                if(data.status == "ok") {
                    $rootScope.user_login = data.login;
                    $rootScope.user_fullName = data.full_name;
                    $rootScope.user_admin = (data.admin == "1");
                    $rootScope.user_loggedin = true;
                    _isLoggedIn = true;
                    _isAdmin = (data.admin == "1");

                    if ((!_isAdmin) && ($location.path().indexOf("/admin") == 0)) {
                        $location.path("/login");
                    }

                    if(callback != null) {
                        callback(true);
                    }

                    // In any way on next step wee need users list
                    getUsersList(noRunMgsWait);

                } else {
                    _isLoggedIn = false;
                    _isAdmin = false;
                    if(allowedUrls.indexOf($location.path()) == -1) {
                        $location.path("/login");
                    }


                    if(callback != null) {
                        callback(false);
                    }
                }

            }, function errorCallback(response) {
                _isLoggedIn = false;
                _isAdmin = false;
                if(allowedUrls.indexOf($location.path()) == -1) {
                    $location.path("/login");
                }
                if(callback != null) {
                    callback(false);
                }
            });
        }
    }


    function login (user, password, callback) {
        $http({
            method: 'POST',
            url: config.apiUrl,
            timeout: 5000,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $.param({
                "action" : "login",
                "login" : user,
                "password" : password,
            })
        }).then(function successCallback(response) {
            var data = response.data;
            if(data.status == "ok") {
                localStorage.setItem('user_id', data.user_id);
                localStorage.setItem('user_key', data.user_key);
                getUserData(callback);
            } else {
                callback(false);
            }

        }, function errorCallback(response) {
            callback(false);
        });
    }

    function getUsersList(noRunMgsWait) {
        $rootScope.user_id = localStorage.getItem('user_id');
        $rootScope.user_key = localStorage.getItem('user_key');

        if (($rootScope.user_id) && ($rootScope.user_key)) {
            $http({
                method: 'POST',
                url: config.apiUrl,
                timeout: 5000,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param({
                    "action" : "getUsersList",
                    "user_id" : $rootScope.user_id,
                    "user_key" : $rootScope.user_key,
                })
            }).then(function successCallback(response) {
                var data = response.data;
                if(data.status == "ok") {
                    $rootScope.users = data.usersList;
                    getLastMessages(noRunMgsWait);
                } else {
                    processRequestError(data);
                }

            }, function errorCallback(response) {

            });
        }
    }

    function getLastMessages(noRunMgsWait) {
        $rootScope.user_id = localStorage.getItem('user_id');
        $rootScope.user_key = localStorage.getItem('user_key');

        if (($rootScope.user_id) && ($rootScope.user_key)) {

            $params = {
                "action" : "getLastMessages",
                "user_id" : $rootScope.user_id,
                "user_key" : $rootScope.user_key
            };

            $http({
                method: 'POST',
                url: config.apiUrl,
                timeout: 5000,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param($params)
            }).then(function successCallback(response) {
                var data = response.data;
                if(data.status == "ok") {
                    msgs = (data.last_messages != []) ? data.last_messages : {};
                    for(var user_id in msgs) {
                        msg = msgs[user_id];
                        if(msg.id != null) {
                            item = {};
                            item[msg['id']] = msg;
                            if(!$rootScope.messages.hasOwnProperty(user_id)) {
                                $rootScope.messages[user_id] = { };
                            }
                            $.extend($rootScope.messages[user_id], item);
                        }
                    }
                    //$.extend($rootScope.messages, $msgs, true);
                    if(!noRunMgsWait) {
                        waitForMessages();
                    }

                } else {
                    processRequestError(data);
                }

            }, function errorCallback(response) {

            });
        }
    }

    function getMessages(target_id, max_id, count) {
        $rootScope.user_id = localStorage.getItem('user_id');
        $rootScope.user_key = localStorage.getItem('user_key');

        if (($rootScope.user_id) && ($rootScope.user_key)) {

            $params = {
                "action" : "getMessages",
                "user_id" : $rootScope.user_id,
                "user_key" : $rootScope.user_key,
                "target_id" : target_id
            };

            if(typeof max_id !== 'undefined') {
                $params.msg_max_id = max_id;
            }

            if(typeof count !== 'undefined') {
                $params.msg_limit = count;
            }

            $http({
                method: 'POST',
                url: config.apiUrl,
                timeout: 5000,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param($params)
            }).then(function successCallback(response) {
                var data = response.data;
                if(data.status == "ok") {
                    msgs = (data.messages != []) ? data.messages : {};
                    if(!$rootScope.messages.hasOwnProperty(target_id)) {
                        $rootScope.messages[target_id] = { };
                    }
                    $.extend($rootScope.messages[target_id], msgs);
                    //$rootScope.messages[target_id] = data.messages;

                } else {
                    processRequestError(data);
                }

            }, function errorCallback(response) {

            });
        }
    }

    function waitForMessages() {
        $rootScope.user_id = localStorage.getItem('user_id');
        $rootScope.user_key = localStorage.getItem('user_key');


        if (($rootScope.user_id) && ($rootScope.user_key)) {
            //console.log(Object.values($rootScope.messages));
            var max_id = 0;
            for (var user_id in $rootScope.messages) {
                var user_max = Math.max.apply(null, Object.keys($rootScope.messages[user_id]));
                max_id = (user_max > max_id) ? user_max : max_id;
            }

            //var max_id = Math.max.apply(null, Object.keys(Object.values($rootScope.messages)));
            //var max_id = Math.max.apply(null, Object.keys(Object.values($rootScope.messages)));
            //var maximum = Math.max.apply(Math, myArr.map(function(o) { return o.x; }));

            $params = {
                "action" : "waitForMessages",
                "user_id" : $rootScope.user_id,
                "user_key" : $rootScope.user_key,
                "msg_min_id" : max_id
            };

            $http({
                method: 'POST',
                url: config.apiUrl,
                timeout: 15000,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param($params)
            }).then(function successCallback(response) {
                var data = response.data;
                if(data.status == "ok") {
                    $msgs = (data.messages != []) ? data.messages : {};
                    for(var msg_id in $msgs) {

                        target_id = ($msgs[msg_id].id_sender == $rootScope.user_id) ? $msgs[msg_id].id_reciever : $msgs[msg_id].id_sender;

                        item = {};
                        item[msg_id] = $msgs[msg_id];

                        if(!$rootScope.messages.hasOwnProperty(target_id)) {
                            $rootScope.messages[target_id] = { };
                        }

                        $.extend($rootScope.messages[target_id], item, true);
                    }

                    //$rootScope.messages[target_id] = data.messages;
                    waitForMessages();

                } else {
                    processRequestError(data);
                }

            }, function errorCallback(response) {

            });
        }
    }

    function sendMessage(target_id, message) {
        $rootScope.user_id = localStorage.getItem('user_id');
        $rootScope.user_key = localStorage.getItem('user_key');

        if (($rootScope.user_id) && ($rootScope.user_key)) {
            $http({
                method: 'POST',
                url: config.apiUrl,
                timeout: 5000,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param({
                    "action" : "sendMessage",
                    "user_id" : $rootScope.user_id,
                    "user_key" : $rootScope.user_key,
                    "target_id" : target_id,
                    "type" : "text",
                    "data" : message
                })
            }).then(function successCallback(response) {
                var data = response.data;
                if(data.status == "ok") {
                    //getMessages(target_id);

                } else {
                    processRequestError(data);
                }

            }, function errorCallback(response) {

            });
        }
    }

    function readMessages(msg_id, user_chat_id) {
        if (($rootScope.user_id) && ($rootScope.user_key)) {
            $http({
                method: 'POST',
                url: config.apiUrl,
                timeout: 5000,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param({
                    "action" : "readMessages",
                    "user_id" : $rootScope.user_id,
                    "user_key" : $rootScope.user_key,
                    "msg_id" : msg_id,
                    "user_chat_id" : user_chat_id
                })
            }).then(function successCallback(response) {
                var data = response.data;
                if(data.status == "ok") {
                    //getMessages(target_id);

                } else {
                    processRequestError(data);
                }

            }, function errorCallback(response) {

            });
        }
    }

    function updateMyProfile(fullName, password, updateKey, callback) {

        if (($rootScope.user_id) && ($rootScope.user_key)) {
            $http({
                method: 'POST',
                url: config.apiUrl,
                timeout: 5000,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param({
                    "action" : "updateMyProfile",
                    "user_id" : $rootScope.user_id,
                    "user_key" : $rootScope.user_key,
                    "new_full_name" : fullName,
                    "new_password" : password,
                    "update_key" : updateKey
                })
            }).then(function successCallback(response) {
                var data = response.data;
                if(data.status == "ok") {
                    getUserData(null, true);
                    if(callback) {
                        callback(true);
                    }
                } else {
                    processRequestError(data);
                }

            }, function errorCallback(response) {

            });
        }
    }

    function createUser(login, full_name, password, active, admin, callback) {
        $rootScope.user_id = localStorage.getItem('user_id');
        $rootScope.user_key = localStorage.getItem('user_key');

        if (($rootScope.user_id) && ($rootScope.user_key) && isAdmin()) {
            $http({
                method: 'POST',
                url: config.apiUrl,
                timeout: 5000,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param({
                    "action" : "createUser",
                    "user_id" : $rootScope.user_id,
                    "user_key" : $rootScope.user_key,
                    "login" : login,
                    "full_name" : full_name,
                    "password" : password,
                    "active" : active,
                    "admin" : admin
                })
            }).then(function successCallback(response) {
                var data = response.data;
                if(data.status == "ok") {
                    getUserData(null, true);
                    if(callback) {
                        callback(true);
                    }

                } else {
                    processRequestError(data);
                }

            }, function errorCallback(response) {

            });
        }
    }

    function updateUser(id, login, full_name, password, active, admin, new_user_key, callback) {
        $rootScope.user_id = localStorage.getItem('user_id');
        $rootScope.user_key = localStorage.getItem('user_key');

        if (($rootScope.user_id) && ($rootScope.user_key) && isAdmin()) {
            $http({
                method: 'POST',
                url: config.apiUrl,
                timeout: 5000,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param({
                    "action" : "updateUser",
                    "user_id" : $rootScope.user_id,
                    "user_key" : $rootScope.user_key,
                    "id" : id,
                    "login" : login,
                    "full_name" : full_name,
                    "password" : password,
                    "active" : active,
                    "admin" : admin,
                    "new_user_key" : new_user_key
                })
            }).then(function successCallback(response) {
                var data = response.data;
                if(data.status == "ok") {
                    getUserData(null, true);
                    if(callback) {
                        callback(true);
                    }

                } else {
                    processRequestError(data);
                }

            }, function errorCallback(response) {

            });
        }
    }

    function deleteUser(id, callback) {
        $rootScope.user_id = localStorage.getItem('user_id');
        $rootScope.user_key = localStorage.getItem('user_key');

        if (($rootScope.user_id) && ($rootScope.user_key) && isAdmin()) {
            $http({
                method: 'POST',
                url: config.apiUrl,
                timeout: 5000,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param({
                    "action" : "deleteUser",
                    "user_id" : $rootScope.user_id,
                    "user_key" : $rootScope.user_key,
                    "id" : id
                })
            }).then(function successCallback(response) {
                var data = response.data;
                if(data.status == "ok") {
                    getUserData(null, true);
                    if(callback) {
                        callback(true);
                    }

                } else {
                    processRequestError(data);
                }

            }, function errorCallback(response) {

            });
        }
    }

    function logout () {
        localStorage.clear();
        _isLoggedIn = false;
        _isAdmin = false;
        $rootScope.user_loggedin = false;
        getUserData(null);
    }

    function isLoggedIn() {
        return _isLoggedIn;
    }

    function isAdmin() {
        return _isAdmin;
    }

    function getServerInfo() {
        $http({
            method: 'POST',
            url: config.apiUrl,
            timeout: 5000,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $.param({
                "action" : "server_info"
            })
        }).then(function successCallback(response) {
            var data = response.data;
            if(data.status == "ok") {
                $rootScope.allow_register = data.info.allow_register;
                $rootScope.new_user_is_active = data.info.new_user_is_active;
                $rootScope.server_name = data.info.server_name;
                $rootScope.admin_email = data.info.admin_email;

            } else {
                processRequestError(data);
            }

        }, function errorCallback(response) {

        });
    }

    function registerUser(login, full_name, password, callback) {
        $http({
            method: 'POST',
            url: config.apiUrl,
            timeout: 5000,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $.param({
                "action" : "register",
                "login" : login,
                "full_name" : full_name,
                "password" : password
            })
        }).then(function successCallback(response) {
            var data = response.data;
            if(data.status == "ok") {
                if(callback) {
                    callback(true);
                }
            } else {
                if(callback) {
                    callback(false);
                }
            }

        }, function errorCallback(response) {
            if(callback) {
                callback(false);
            }
        });
    }

    getServerInfo();
    getUserData(null);

    $rootScope.$watch('messages', function (data, oldData) {
        $rootScope.unreadChats = 0;
        $rootScope.unreadMessages = 0;
        for(var userId in $rootScope.users) {
            if ($rootScope.messages.hasOwnProperty(userId)) {
                $rootScope.users[userId].lastMessage = $rootScope.messages[userId][Math.max.apply(null, Object.keys($rootScope.messages[userId]))];
                $rootScope.users[userId].unreadMessages = 0;
                angular.forEach( $rootScope.messages[userId], function(item){
                    if((item.id_reciever == $rootScope.user_id) && (item.read_time == null)) {
                        $rootScope.users[userId].unreadMessages++;
                        $rootScope.unreadMessages++;
                    }
                });
                if($rootScope.users[userId].unreadMessages > 0) {
                    $rootScope.unreadChats++;
                }
            } else {
                $rootScope.users[userId].lastMessage = null;
            }
        }
        //console.log(data);
    }, true);

    $rootScope.$watch('users', function (data, oldData) {
        //console.log(data);
    }, true);


    return {
        demo: demo,
        login: login,
        logout: logout,
        isLoggedIn: isLoggedIn,
        isAdmin : isAdmin,
        updateMyProfile: updateMyProfile,

        getMessages: getMessages,
        sendMessage: sendMessage,
        waitForMessages: waitForMessages,
        readMessages: readMessages,

        createUser: createUser,
        updateUser : updateUser,
        deleteUser : deleteUser,
        getServerInfo : getServerInfo,
        registerUser : registerUser

    };
});
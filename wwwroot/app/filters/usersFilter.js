app.filter('usersFilter', function() {
    return function(users, data) {
        if((!data) || (data == "")) {
            return users;
        }
        result = {};

        for(var userId in users) {
            if((users[userId].full_name.indexOf(data) >= 0) ||
                (users[userId].login.indexOf(data) >= 0)) {
                result[userId] = users[userId];
            }
        }
        return result;
    };
});
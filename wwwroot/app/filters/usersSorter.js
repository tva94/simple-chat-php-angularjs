app.filter('usersSorter', function(){
    return function(input) {
        if (!angular.isObject(input)) return input;

        var array = [];
        for(var objectKey in input) {
            array.push(input[objectKey]);
        }

        array.sort(function(a, b){
            if(a.lastMessage) {
                if(b.lastMessage) {
                    return b.lastMessage.id - a.lastMessage.id;
                } else {
                    return -1;
                }
            } else {
                if(b.lastMessage) {
                    return 1;
                } else {
                    return (a.full_name.localeCompare(b.full_name));
                }
            }
        });
        return array;
    }
});

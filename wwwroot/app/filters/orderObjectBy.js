app.filter('orderObjectBy', function(){
    return function(input, attribute, inverse) {
        if (!angular.isObject(input)) return input;
        inverse = typeof inverse !== 'undefined' ? inverse : false;

        var array = [];
        for(var objectKey in input) {
            array.push(input[objectKey]);
        }

        array.sort(function(a, b){
            a = parseInt(a[attribute]);
            b = parseInt(b[attribute]);
            return !inverse ? (a - b) : (b - a);
        });
        return array;
    }
});

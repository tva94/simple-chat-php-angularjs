app.controller("navbarController", function ($scope, $rootScope, $routeParams, $location, ChatCore) {
    $scope.isLoggedIn = false;
    $scope.allowRegister = false;
    $scope.isAdmin = false;
    $scope.unreadChats = 0;

    $scope.logout = function () {
        ChatCore.logout();
        $location.path('/');
    };

    $scope.test = function () {
        //console.log(Sha512.hash('12345'));
        //console.log($scope);
        //console.log($rootScope);
        /*console.log($.param({
            "action" : "massSet",
            "id_value_list" : {"a" : "b"}
        }));*/
    };

    $scope.$watch(
        function() { return $rootScope.user_fullName },
        function(user_fullName) {
            $scope.user = user_fullName;
        }
    );

    $scope.$watch(
        function() { return $rootScope.user_loggedin },
        function(user_loggedin) {
            $scope.isLoggedIn = user_loggedin;
        }
    );

    $scope.$watch(
        function() { return $rootScope.user_admin },
        function(user_admin) {
            $scope.isAdmin = user_admin;
        }
    );

    $scope.$watch(
        function() { return $rootScope.unreadChats },
        function(unreadChats) {
            $scope.unreadChats = unreadChats;
        }
    );

    $scope.$watch(
        function() { return $rootScope.allow_register },
        function(allow_register) {
            $scope.allowRegister = allow_register;
        }
    );
});

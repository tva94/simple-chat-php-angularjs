app.config(['$routeProvider', function($routeProvider) {

    $routeProvider
        .when("/", {
            templateUrl : "app/pages/index.html"
        })
        .when("/login", {
            templateUrl : "app/pages/login.html",
            controller: "loginController"
        })
        .when("/register", {
            templateUrl : "app/pages/register.html",
            controller: "registerController"
        })
        .when("/chats", {
            templateUrl : "app/pages/chats.html",
            controller: "chatsController"
        })
        .when("/chats/:chatId", {
            templateUrl : "app/pages/chats.html",
            controller: "chatsController"
        })
        .when("/profile", {
            templateUrl : "app/pages/profile.html",
            controller: "profileController"
        })
        .when("/admin", {
            templateUrl : "app/pages/admin.html",
            controller: "adminController"
        })
        .when("/admin/:userId", {
            templateUrl : "app/pages/admin.html",
            controller: "adminController"
        })
        .otherwise("/");

}]);
